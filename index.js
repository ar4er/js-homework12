// Чому для роботи з input не рекомендується використовувати клавіатуру?
// e.code всегда привязан к физическим местам кнопок. Лучше использовать e.key.
// e.key ,в свою очередь, привязан к выбранному языку.
// input лучше проверять "change".

const btn = document.querySelectorAll(".btn");
const btnArray = [...btn];

document.body.addEventListener("keypress", (e) => {
  btnArray.forEach((element) => {
    let eValue;
    element.style.backgroundColor = "black";
    
    if (e.code.includes("Key")) eValue = e.code.slice(3);
    else eValue = e.code;
    console.log(eValue);
    console.log(e.code);
    if (eValue === element.innerText) element.style.backgroundColor = "blue";
  });
});
